from django.db.models import Q
from django.test import TestCase

from tests.models import CheckModel, CorrectModel


class ModelTestCases(TestCase):
    @classmethod
    def setUpTestData(cls):
        fk_column = CheckModel.objects.create(id=1, name="fk_column")
        CorrectModel.objects.create(id=1, fk_column=fk_column, name="test")

    def test_correct_versions(self):
        CorrectModel.objects.get(fk_column=1, id=1)
        set(CorrectModel.objects.filter(fk_column=1))
        set(CorrectModel.objects.filter(fk_column_id=1))
        set(CorrectModel.objects.filter(fk_column=1).exclude(fk_column=1))
        set(CorrectModel.objects.filter(fk_column_id=1).exclude(fk_column_id=1))
        set(CorrectModel.objects.filter(id=1).filter(name=1).filter(fk_column=1))
        set(CorrectModel.objects.filter(id=1).filter(name=1).filter(fk_column_id=1))
        set(CorrectModel.objects.filter(fk_column=1).exclude(id=1, name=1))
        set(CorrectModel.objects.filter(fk_column_id=1).exclude(id=1, name=1))

        set(CorrectModel.objects.filter(Q(fk_column=1)))
        set(CorrectModel.objects.filter(Q(fk_column=1) & Q(name=1)))
        set(CorrectModel.objects.filter(name=1).filter(Q(fk_column=1) & ~Q(name=1)))

        set(CorrectModel.objects.complex_filter({"fk_column": 1}))

    def test_wrong_versions(self):
        with self.assertRaises(AttributeError):
            set(CorrectModel.objects.exclude(fk_column=1))

        with self.assertRaises(AttributeError):
            CorrectModel.objects.get(id=1)

        with self.assertRaises(AttributeError):
            set(CorrectModel.objects.filter(fk_column__name=1))

        with self.assertRaises(AttributeError):
            set(CorrectModel.objects.filter(~Q(fk_column=1)))

        with self.assertRaises(AttributeError):
            """
            Correct query but too complex to hack.
            """
            set(CorrectModel.objects.exclude(~Q(fk_column=1)))

        with self.assertRaises(AttributeError):
            set(CorrectModel.objects.filter(id=1, name=1).exclude(fk_column=1))

        with self.assertRaises(AttributeError):
            set(CorrectModel.objects.filter(Q(fk_column=1) | Q(name=1)))

        with self.assertRaises(AttributeError):
            set(CorrectModel.objects.values("name"))
