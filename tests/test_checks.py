from django.apps import apps
from django.core.checks.messages import Error
from django.test import SimpleTestCase

from query_check.checks import query_filter_check
from tests.models import WrongManagerModel, WrongModel


class ChecksTestCases(SimpleTestCase):
    def test_checks(self):
        self.assertEqual(
            query_filter_check(app_configs=apps.get_app_config("tests")),
            [
                Error(
                    "Wrong parent model class",
                    hint="Use BaseModel instead django.db.models.Model",
                    obj=WrongModel,
                    id="tests.E002",
                ),
                Error(
                    "Wrong parent manager class",
                    hint="Use BaseManager instead django.db.models.Manager",
                    obj=WrongManagerModel,
                    id="tests.E003",
                ),
            ],
        )
