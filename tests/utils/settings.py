import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = "fake-key"
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}

INSTALLED_APPS = ("tests",)

QUERY_CHECK_LOCAL_APPS = ["tests"]
QUERY_CHECK_MODELS = ["tests.models.CheckModel"]


SILENCED_SYSTEM_CHECKS = ["tests.E002", "tests.E003"]
