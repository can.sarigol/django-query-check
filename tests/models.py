from django.db import models
from django.db.models.manager import BaseManager

from query_check.base.models import BaseModel


class CheckModel(models.Model):
    name = models.CharField(max_length=100)


class CorrectModel(BaseModel):
    fk_column = models.ForeignKey(CheckModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)


class WrongModel(models.Model):
    fk_column = models.ForeignKey(CheckModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)


class WrongManagerModel(BaseModel):
    fk_column = models.ForeignKey(CheckModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    objects = BaseManager()
