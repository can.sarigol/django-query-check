# Query Check

## Install
```bash
pip install django-query-check
```
## Usage
* Add app in installed apps.
```python
INSTALLED_APPS = [
    ...
    "query_check",
]
```
* Adjust settings
```python
QUERY_CHECK_LOCAL_APPS = ["tests"]
QUERY_CHECK_MODELS = ["tests.models.CheckModel"]
# Optional (If it is not defined in settings, QUERY_CHECK_MODELS is used)
QUERY_CHECK_EXCEPTIONAL_MODELS = ["tests.models.ExceptionalModel"]
```

### Check model base class
![](/images/image1.png)
### Check filters whether or not they have client attribute
Sample view code
```python
def index(request):
    CorrectModel.objects.filter(name="test")
    return render(request, "index.html", {})
```
![](/images/image2.png)
