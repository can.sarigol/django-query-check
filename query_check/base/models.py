from django.conf import settings
from django.db import models
from django.db.models.expressions import Col


class BaseQuerySet(models.QuerySet):
    def _is_model_in_filter(self):
        where_clauses = self.query.where
        for model in settings.QUERY_CHECK_MODELS:
            if not (
                where_clauses.connector == "AND"
                and where_clauses.negated is False
                and any(
                    kid
                    for kid in where_clauses.children
                    if hasattr(kid, "lhs")
                    and isinstance(kid.lhs, Col)
                    and kid.lhs.target.related_model
                    and (
                        f"{kid.lhs.target.related_model.__module__}"
                        f".{kid.lhs.target.related_model.__name__}"
                    )
                    == model
                )
            ):
                raise AttributeError(f"'{model}' filter is missing")

    def _fetch_all(self):
        self._is_model_in_filter()
        super()._fetch_all()


class BaseManager(models.Manager):
    def get_queryset(self):
        return BaseQuerySet(self.model, using=self._db)


class BaseModel(models.Model):

    objects = BaseManager()

    class Meta:
        abstract = True
