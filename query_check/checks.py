from django.apps import apps
from django.conf import settings
from django.core.checks import Error, register

from query_check.base.models import BaseManager


@register
def query_filter_check(app_configs, **kwargs):
    errors = []
    from query_check.base.models import BaseModel

    if not hasattr(settings, "QUERY_CHECK_LOCAL_APPS") or not hasattr(
        settings, "QUERY_CHECK_MODELS"
    ):
        return [
            Error(
                "'QUERY_CHECK_LOCAL_APPS' and 'QUERY_CHECK_MODELS'"
                " should be placed in settings",
                obj=None,
                id="query_check.E001",
            )
        ]

    exceptional_models = settings.QUERY_CHECK_MODELS
    if hasattr(settings, "QUERY_CHECK_EXCEPTIONAL_MODELS"):
        exceptional_models = settings.QUERY_CHECK_EXCEPTIONAL_MODELS

    for app_label in settings.QUERY_CHECK_LOCAL_APPS:
        for model in apps.get_app_config(app_label).get_models():
            if f"{model.__module__}.{model.__name__}" in exceptional_models:
                continue

            if model.mro()[1] != BaseModel:
                errors.append(
                    Error(
                        "Wrong parent model class",
                        hint="Use BaseModel instead django.db.models.Model",
                        obj=model,
                        id=f"{app_label}.E002",
                    )
                )
            elif type(model._meta.managers[0]) != BaseManager:
                errors.append(
                    Error(
                        "Wrong parent manager class",
                        hint="Use BaseManager instead django.db.models.Manager",
                        obj=model,
                        id=f"{app_label}.E003",
                    )
                )

    return errors
